---
jupyter:
  jupytext:
    cell_metadata_filter: -all
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.2'
      jupytext_version: 1.3.2
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

# CFM 2020: Getting Started


## Packages

```python
import numpy as np
import pandas as pd

pd.options.display.max_rows = 200

import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
```

## Data Description

```python
train_without_label = pd.read_hdf("data/train_dc2020.h5")
train_labels = pd.read_csv("data/train_labels.csv")
```

```python
train = train_without_label.merge(train_labels, on="ID")
```

The dimension of the dataset is:

```python
train.shape
```

The features of the dataset are the following:

For each order book, 13 cols

* ask
* ask1
* ask_size
* ask_size1
* bid* (same as ask)
* price
* qty
* source_id
* tod
* ts_last_update

That sums up to 78 columns
The remaining 16 colums are historical data about the trader
those are the 4 last operations of the trader

* price
* qty
* source_id
* tod

Furthermore 3 context cols

* product_id
* day_id
* row_id

Using that information we add up a TARGET column merging the
label dataset on the row_id
