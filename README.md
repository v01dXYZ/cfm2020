# CFM Challenge 2020

> **Disclaimer**
>
> This is a Work in Progress (*WIP*)
> 
> My main activity is to learn / research about CPU architecture
> and OpenCL to better leverage my hardware, with no immediate results
> (but hopefully huge benefits later).

## Official Webpage

https://challengedata.ens.fr/challenges/40

## How to

Use `jupytext` to generate the Notebooks from the Markdown files.
