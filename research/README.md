# [WIP] Research

Efficient computations is a difficult topic with the modern Computer Architectures
we have today.

That's quite a large and vivid topic. The main focus being implementation, 
the best way to get into that is to use and understand the following technologies.

* OpenCL
* BLAS / LAPACK

In order to do so, some useful references:

* *Computer Systems: A Programmer's Perspective* Bryant
* *Computer Organization and Design RISC-V* Patterson, Hennessy
* The BLIS and FLAME project
* *Heteregeneous Computing with OpenCL 2.0* Kaeli, Mistry, Schaa, Zhang

Furthermore, other useful technologies to feed the algorithms:

* HDF5
* Apache Arrow
* Sleef

and to get better with unit testing and gdb.

The focus is right now to implement a *quantile regression algorithm* (since
the smooth optimizations field is vaster and would need more time to study).
