# Quantile Regression

Quantile Regression is a simple example of a non-smooth loss function.
A interesting paper/implementation to solve large dimensional quantile
regression problem was published by Eric (Congrui) Yi: http://arxiv.org/abs/1509.02957.
The source code is availabe at https://github.com/CY-dev/hqreg.

Furthermore, the proximal framework seems suited to such a problem.

* *Proximal Algorithms in Statistics and Machine Learning* by Poison, Scott,
  Willard https://projecteuclid.org/download/pdfview_1/euclid.ss/1449670858
