import numpy as np
import tables

train = tables.open_file("data/train_dc2020.h5", "r")

block0_values = train.root.data.block0_values
array = block0_values.read()
flat_arr = array.flatten()

subsize = 10
subarr = flat_arr[:subsize]
arr_sum = np.nansum(subarr)

print("Numpy Sum", arr_sum)
