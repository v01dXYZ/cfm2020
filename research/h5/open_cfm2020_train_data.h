#pragma once

#include <hdf5.h>

void
open_cfm2020_train_data(float ** output_array,
			hsize_t * output_size,
			hsize_t * shape);
