#include <stdlib.h>

#include <hdf5.h>

void
open_cfm2020_train_data(float ** output_array,
			hsize_t * output_size,
			hsize_t * shape)

{
	hid_t file_id = H5Fopen("data/train_dc2020.h5",
				H5F_ACC_RDONLY,
				H5P_DEFAULT);
	hid_t dataset_id = H5Dopen1(file_id,
				    "/data/block0_values");

	hid_t space_id = H5Dget_space(dataset_id);
	hsize_t size = H5Sget_simple_extent_npoints(space_id);

	H5Sget_simple_extent_dims(space_id, shape, NULL);

	float * array = (float *) malloc(size * sizeof(float));

	H5Dread(dataset_id,
		H5T_NATIVE_FLOAT,
		H5S_ALL,
		H5S_ALL,
		H5P_DEFAULT,
		array);

	H5Dclose(dataset_id);
	H5Sclose(space_id);
	H5Fclose(file_id);

	*(output_array) = array;
	*(output_size) = size;
}
