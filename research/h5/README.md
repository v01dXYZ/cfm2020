# HDF5 

Ressource to get started with HDF5: document published by the HDF
Group named *High Level Introduction to HDF5*.

This format is vey versatile and Pandas actually has implemented
its own data format ontop of HDF5 (as explained in the following
post https://stackoverflow.com/questions/33641246/pandas-cant-read-hdf5-file-created-with-h5py/33644128#33644128).

Two packages to handle HDF5 files with Python: `h5py` and `pytables`.
Their respective FAQ explains the differences between the two packages.

## Exploring a hdf5 file using `pytables`

> Reference: https://pytables.org/userguide/tutorials.html

We import the package

```python
import numpy as np
import tables
```

We can now open the file

```python
train = tables.open_file("data/train_dc2020.h5", "r")
```

We can iterate over a `File`, scanning the HDF objects it contains.
If we want to walk the nodes of only a subset of those objects,
we can also use the `walk_nodes` method, which returns an iterator.
Down here, we scan starting from the root and getting only
the objects which are `Array`.

```python
for array in train.walk_nodes("/", "Array"):
    print("Accessing the node named:", array.name)
    print("With the atttributes:", array.attrs)
    print("Array dtype and dimension:", array.dtype, array.shape)
```

We can now explore the Pandas HDF format.
Reading the file hierarchy, we have a `data` group with the following
children:

* `axis0` 97 column names
* `axis1` index (numpy array)
* `block0_items` 68 column names
* `block0_values` float32 values (959506, 68)
* `block1_items` 10 column names (numpy array)
* `block1_values` int32 values (959506, 10)
* `block2_items` 18 column names (numpy array)
* `block2_values` int64 values (959506, 18)
* `block3_items` 1 column names (numpy array)
* `block3_values` float64 values (959506, 1)

We can easily check that the sum of the length of the column names in
the `block?_items` equals the length of the column names in `axis0`.

Let's select the `block0_values` dataset and get a numpy array:

```python
block0_values = train.root.data.block0_values
array = block0_values.read()
```

## HDF C API

> Reference: HDF5 C/FORTRAN Reference Manual

* `list_group_objects.c`: insired from the official example `h5_info.c`, list the children of a given group.
* `read_dataset.c`: read a array dataset and sum the 10 first values.
