#include <stdio.h>
#include <hdf5.h>

void scan_group(hid_t group_id);

int main(int argc, char ** argv)
{
	char * filepath = "data/train_dc2020.h5";

	/* To create a file we use a H5F routineo
	 * - H5F_ACC_RDWR: File Access Read & Write
	 * - H5P_DEFAULT: Propertie List Default
	 */
	hid_t file = H5Fopen("data/train_dc2020.h5",
			      H5F_ACC_RDONLY,
			      H5P_DEFAULT);

	char * grouppath = "/";
	
	if(argc == 1)
	{
		printf("No argument, Looking at the root /\n");
		fflush(stdout);
	} else {
		grouppath = argv[1];
	}

	/* To access the group we use a H5G routine
	 */
	hid_t grp = H5Gopen1(file, grouppath);

	scan_group(grp);

	int status = H5Fclose(file);

	return 0;
}

void scan_group(hid_t group_id)
{
	char group_name[1024];

	ssize_t length = H5Iget_name(group_id, group_name, 1024);

	printf("The name of the group is %s\n", group_name);

	hsize_t objects_length;
	herr_t err = H5Gget_num_objs(group_id, &objects_length);

	printf("The number of objects is %llu\n", objects_length);

	for(int i=0; i < objects_length; i+=1)
	{
		char memb_name[1024];
		printf("    Member %d\n", i);
		length = H5Gget_objname_by_idx(group_id,
					       (hsize_t) i,
					       memb_name,
					       1024);

		printf("    Name: %s\n", memb_name);

		int obj_type = H5Gget_objtype_by_idx(group_id,
						     (size_t) i);

		printf("    Type: %d\n", obj_type);
 	}
}
