#include <stdio.h>
#include <stdlib.h>

#include <math.h>

#include <hdf5.h>

void display_type_class(H5T_class_t t_class);
float naive_summation(float * array, size_t size);
void array_head(float * array, size_t head_size);

int main()
{
  /* 1 - open HDF5 file
   * 2 - open the dataset using by path
   * 3 - get the dataspace, query the rank, dims, size
   * 4 - get the datatype
   * 5 - read the dataset
   * 6 - naively sum the n-first values
   * 7 - free buffers and HDF5 ressources
   */

  // 1 - open HDF5 file
  hid_t file_id = H5Fopen("data/train_dc2020.h5",
		       H5F_ACC_RDONLY,
		       H5P_DEFAULT);

  // 2 - open the dataset using by path
  char * dataset_path = "/data/block0_values";
  hid_t dataset_id = H5Dopen1(file_id, dataset_path);
  printf("Dataset id: %ld\n", dataset_id);

  // 3 - get the dataspace, query the rank, dims, size
  hid_t space_id = H5Dget_space(dataset_id);

  int ndims = H5Sget_simple_extent_ndims(space_id);
  hsize_t dims[ndims];
  H5Sget_simple_extent_dims(space_id, dims, NULL);
  printf("ndims: %d\n", ndims);

  for(int i=0; i<ndims; i+=1) {
    printf("dim %d: %llu \n",i, dims[i]);
  }

  hsize_t size = H5Sget_simple_extent_npoints(space_id);
  printf("size: %llu\n", size);

  // 4 - get the datatype
  hid_t type_id = H5Dget_type(dataset_id);
  H5T_class_t t_class = H5Tget_class(type_id);
  display_type_class(t_class);

  // 5 - read the dataset
  printf("Reading DataSet\n");
  float * array = malloc(size*sizeof(float));

  H5Dread(dataset_id,	/* dataset id */
	  H5T_NATIVE_FLOAT, /* mem_type */
	  H5S_ALL,	  /* mem_space_id */
	  H5S_ALL,	  /* file_space_id */
	  H5P_DEFAULT,	  /* propertie list) */
	  array);	  /* address of the array */

  printf("Printing the first elements of the array\n");
  const size_t head_size = 10;
  array_head(array, head_size);

  // 6 - naively sum the n-first values
  float sum = naive_summation(array, head_size);
  printf("Naive summation: %.10f\n", sum);

  // 7 - free buffers and HDF5 ressources
  printf("Closing Resources, DataSet, File\n");
  free(array);
  H5Tclose(type_id);
  H5Sclose(space_id);
  H5Dclose(dataset_id);
  H5Fclose(file_id);

  return 0;
}


// Directly from the HDF5 examples
void display_type_class(H5T_class_t t_class)
{
  if(t_class == H5T_INTEGER) {
    puts(" Datatype is 'H5T_INTEGER'.\n");
    /* display size, signed, endianess, etc. */
  } else if(t_class == H5T_FLOAT) {
    puts(" Datatype is 'H5T_FLOAT'.\n");
    /* display size, endianess, exponennt, etc. */
  } else if(t_class == H5T_STRING) {
    puts(" Datatype is 'H5T_STRING'.\n");
    /* display size, padding, termination, etc. */
  } else if(t_class == H5T_BITFIELD) {
    puts(" Datatype is 'H5T_BITFIELD'.\n");
    /* display size, label, etc. */
  } else if(t_class == H5T_OPAQUE) {
    puts(" Datatype is 'H5T_OPAQUE'.\n");
    /* display size, etc. */
  } else if(t_class == H5T_COMPOUND) {
    puts(" Datatype is 'H5T_COMPOUND'.\n");
    /* recursively display each member: field name, type  */
  } else if(t_class == H5T_ARRAY) {
    puts(" Datatype is 'H5T_COMPOUND'.\n");
    /* display  dimensions, base type  */
  } else if(t_class == H5T_ENUM) {
    puts(" Datatype is 'H5T_ENUM'.\n");
    /* display elements: name, value   */
  } else  {
    puts(" Datatype is 'Other'.\n");
    /* and so on ... */
  }
}

float naive_summation(float * array, size_t size) {
  float sum = 0;
  for(size_t i=0; i<size; i+=1)
    {
      float element = array[i];
      if(isnan(element))
	{
	  // Simply ignore nan values
	  continue;
	}

      sum += element;
    }

  return sum;
}

void array_head(float * array, size_t head_size) {
  for(size_t i=0; i<head_size; i+=1)
    {
      printf("(%lu) %f \n", i, array[i]);
    }
}
