#include <CL/cl2.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include <memory>
#include <algorithm>

// Log Message Display Counter
int display_counter = 0;

void log_msg(std::string msg){
  std::cout << "[" << display_counter++ << "] " << msg << std::endl;
}

cl::Platform find_platform_with_cl2_support()
{
  log_msg("Finding Platform with OpenCL 2 support");
  std::vector<cl::Platform> platforms;
  cl::Platform::get(&platforms);

  cl::Platform platform_cl2;

  for(auto &platform: platforms) {
    std::string platform_version = platform.getInfo<CL_PLATFORM_VERSION>();
    std::cout << "Platform:" << platform_version << std::endl;
    if (platform_version.find("OpenCL 2.") != std::string::npos) {
      platform_cl2 = platform;
    }
  }

  if (platform_cl2() == 0) {
    std::cout << "No OpenCL 2.0 platform found." << std::endl;
  }

  return platform_cl2;
}

std::vector<cl::Device> find_devices_from_platform(cl::Platform platform)
{
  log_msg("Finding the devices tied to the platform");
  std::vector<cl::Device> devices;
  platform.getDevices(CL_DEVICE_TYPE_ALL, &devices);

  std::cout << "Number of device:" << devices.size() << std::endl;
  for(auto &device: devices) {
    std::cout << "* Device: " << device.getInfo<CL_DEVICE_NAME>() << std::endl;
  }

  return devices;
}

cl::Program create_program_from_source(std::string file_path, cl::Context context)
{
  log_msg("Fetching the Program Source");
  std::ifstream vecadd_source_file(file_path,
				   std::ifstream::binary);

  std::stringstream vecadd_source_stream_buffer;
  vecadd_source_stream_buffer << vecadd_source_file.rdbuf();

  std::string vecadd_source_string(vecadd_source_stream_buffer.str());

  std::cout << "Program Source:" << std::endl;
  std::cout << vecadd_source_string;
  std::cout << "--------------------" << std::endl;

  std::vector<std::string> program_str_vec {vecadd_source_string};

  log_msg("Compiling the Program");
  cl::Program program(context, vecadd_source_string, true);
  program.build("-cl-std=CL2.0");


  log_msg("Error of the Program Build");
  cl_int build_error = CL_SUCCESS;
  auto build_info = program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(&build_error);

  for(auto &pair: build_info) {
    std::cout << "    Error (if any):" << pair.second << std::endl;
  }

  return program;
}

void display_output(std::vector<int> output)
{
  log_msg("Printing out the output");
  std::cout << output.size() << std::endl;
  for(int i=0; i<output.size(); i+=1) {
    auto element = output[i];
    std::cout << "(" << i << ") " << element << std::endl;
  }
}

int main()
{
  // OpenCL Hello World
  // 1 - Detecting Platform with OpenCL 2 support
  // 2 - Getting the devices
  // 3 - Creating the context for the devices
  // 4 - Creating the command queue for each
  //     device
  // 5 - Fetching the Program Sources and Compiling the Program
  // 6 - Preparing Buffers and Transfering Data
  // 7 - Inkoking Kernel
  // 8 - Transfering back the output data

  auto platform_cl2 = find_platform_with_cl2_support();
  auto devices = find_devices_from_platform(platform_cl2);

  cl::Context context(devices);

  log_msg("Setting up the command queue");
  cl::CommandQueue command_queue(context,
				 devices[0],
				 cl::QueueProperties::Profiling
				 | cl::QueueProperties::OutOfOrder);

  auto program = create_program_from_source("vecadd.cl", context);

  auto vecadd = cl::KernelFunctor<cl::Buffer,cl::Buffer,cl::Buffer>(program, "vecadd");

  log_msg("Setting up the Host Buffer");
  const int vec_size = 5;

  std::vector<int>
    host_input_A(vec_size, 5),
    host_input_B(vec_size, 5),
    host_output_C(vec_size, -1);

  log_msg("Creating the device buffers");
  cl::Buffer input_A(context, host_input_A.begin(), host_input_A.end(), true);
  cl::Buffer input_B(context, host_input_A.begin(), host_input_A.end(), true);
  cl::Buffer output_C(context, CL_MEM_WRITE_ONLY, 5*sizeof(int));

  log_msg("Invoking VecAdd Kernel");
  auto vecadd_event = vecadd(cl::EnqueueArgs(command_queue,
					     vec_size),
			    input_A,
			    input_B,
			    output_C);

  log_msg("Copying Device ouput buffer to Host");
  cl::copy(command_queue,
	   output_C,
	   host_output_C.begin(),
	   host_output_C.end());

  display_output(host_output_C);

  return 0;
}
