#include <stdio.h>
#include <stdlib.h>

#include <math.h>

#include <hdf5.h>
#include <blis/blis.h>


#include "../../h5/open_cfm2020_train_data.h"

int main()
{
	/* 1 - Load the data into a malloc allocated buffer
	 * 2 - Defining the properties of the Matrix
	 * 3 - Creating the buffer and attaching the buffer
	 *     with the right properties
	 * 4 - Sum the matrix using Level 2 Matrix to Vect
	 *     and Level 1 Vect to Vect operations
	 * 5 - Free the ressources
	 */

	printf("Loading the (partial) train data\n");

	float * array_buffer;
	hsize_t size;
	hsize_t shape[2];
	open_cfm2020_train_data(&array_buffer, &size, shape);

	printf("size: %llu dimensions: (%llu, %llu)\n", size, shape[0], shape[1]);

	printf("Fill the nan values with zeros\n");
	float naive_sum = 0;
	hsize_t na_value_counts = 0;
	for(hsize_t i=0; i<size; i+=1) {
		float element = array_buffer[i];
		if(isnan(element)) {
			array_buffer[i] = 0;
			na_value_counts += 1;
		} else {
			naive_sum += element;
		}
	}

	printf("Naive sum: %f with %llu missing values\n", naive_sum, na_value_counts);

	dim_t datatype = BLIS_FLOAT;
	dim_t row_stride = shape[1]; // Row major Format
	dim_t col_stride = 1;

	obj_t array;
	bli_obj_create_with_attached_buffer(datatype,
					    shape[0], // m: # rows
					    shape[1], // n: # cols
					    array_buffer, // buffer to attach
					    row_stride,
					    col_stride,
					    &array); // object to create

	/* Vectors to store the following operations:
	 * 1: vect_x := 1
	 * 2: vect_y := array * vect_x
	 * 3: vect_z := 1
	 * 4: output := vect_z * vect_y
	 */
	obj_t vect_x, vect_y, vect_z, output;

	bli_obj_create(datatype,
		       shape[1],
		       1,
		       0,
		       0,
		       &vect_x);
	bli_obj_create(datatype,
		       shape[0],
		       1,
		       0,
		       0,
		       &vect_y);
	bli_obj_create(datatype,
		       1,
		       shape[0],
		       0,
		       0,
		       &vect_z);
	bli_obj_create_1x1(datatype,
			   &output);

	bli_setv(&BLIS_ONE, &vect_x);
	bli_setv(&BLIS_ONE, &vect_z);

	bli_gemv(&BLIS_ONE,
		 &array,
		 &vect_x,
		 &BLIS_ZERO,
		 &vect_y);

	bli_dotv(&vect_z, &vect_y, &output);

	// Displaying output
	bli_printm("output", &output, "%f", "");

	// Freeing the ressources
	bli_obj_free(&vect_x);
	bli_obj_free(&vect_y);
	bli_obj_free(&vect_z);
	bli_obj_free(&output);

	free(array_buffer);

	return 0;
}
