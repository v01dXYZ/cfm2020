# BLIS

BLIS is a modular linear algebra engine implementing and extending the
features of BLAS. This project is extremely promising for both its performance
and the educative ressources surrounding it (backed University of Texas - Austin).
Furthermore, its design is easier to handle and the backward compatibility is not
as much a concern than with BLAS.

There are two APIs we can use a typed BLAS-alike one and a object based one.
For simplicity sake and easing the learning curb, we shall use the object one.

## HOW TO

In order to use libblis without installing it system-wise, you have to put
the following links to the following folders (with `DESTDIR` the destination dir
of the `make install DESTDIR=$DESTDIR` you used with BLIS):

* `$DESTDIR/lib/` the shared object file exported
* `$DESTDIR/include` the header directory exported


## References

* **BLIS project page.** https://github.com/flame/blis
* **BLIS Object API** https://github.com/flame/blis/blob/master/docs/BLISObjectAPI.md
* **LAFF - On Programming for High Performance** https://ulaf.net
